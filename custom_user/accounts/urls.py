from django.conf.urls import url
from accounts.views import home

urlpatterns = [
    url(r'^home/$', home.as_view(), name='home'),
]
