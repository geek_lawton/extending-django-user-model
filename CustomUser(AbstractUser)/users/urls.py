from django.conf.urls import url, include
from users.views import home,Signup
#from django.views.generic.base import TemplateView

urlpatterns = [
    #url(r'^home/$', TemplateView.as_view(template_name='users/home.html'), name='home'),
    url(r'^home/$', home.as_view(), name='home'),
    url(r'^Signup/$', Signup.as_view(), name='Signup'),
]
